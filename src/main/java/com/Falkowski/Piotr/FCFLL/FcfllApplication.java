package com.Falkowski.Piotr.FCFLL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FcfllApplication {

	public static void main(String[] args) {
		SpringApplication.run(FcfllApplication.class, args);
	}

}
